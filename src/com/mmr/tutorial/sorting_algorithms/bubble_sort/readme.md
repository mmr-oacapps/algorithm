## Data process from start to end or left to right. 

### Performance 

##### Worst case performance : O(n<sup>2</sup>) 
Not appropriate for large unsorted data sets 

##### Average case performance: O(n<sup>2</sup>) 
Not appropriate for large unsorted data sets 

##### Best case performance: O(n) 
Very good best performance and can efficiently sort small and nearly sorted data sets 

##### Space required: O(N) 
Bubble sort operates directly on the input array meaning it is a candidate algorithm when minimizing space is paramount 

### Algorithm

We assume list is an array of n elements. We further assume that swap function swaps the values of the given array elements. 

````
begin BubbleSort(list)  

    for all elements of list  
        if list[i] > list[i+1]  
            swap(list[i], list[i+1])  
        end if  
    end for  

    return list  

end BubbleSort 
````