package com.mmr.tutorial.sorting_algorithms.bubble_sort;

import java.util.Arrays;

public class BubbleSort {

    public static void main(String[] args) {
        int array[] = {3, 8, 2, 1, 5, 4, 6, 7};

        System.out.println("Before Sort : " + Arrays.toString(array));

        bubbleSort(array);

        System.out.println("After Sort : " + Arrays.toString(array));
    }

    static void bubbleSort(int[] array)
    {
        int length = array.length;
        int temp = 0;

        /** Looping through the array **/
        for(int i=0; i < length; i++)
        {
            for(int j=1; j < (length-i); j++)
            {
                /** if value is greater then previous value **/
                if(array[j-1] > array[j])
                {
                    /** swap elements **/

                    /** put value of position (j-1)  into temporary variable **/
                    temp = array[j-1];

                    /** now put value of position (j) into position (j-1) **/
                    array[j-1] = array[j];

                    /** finally,  put temp value into position (J) **/
                    array[j] = temp;
                }

            }
        }

    }

}
