## Data process from start to end or left to right. 
 
Insertion sort is the sorting mechanism where the sorted array is built having one item at a time.

### Insertion Sort works as follows:
1. The first step involves the comparison of the element in question with its adjacent element.
2. And if at every comparison reveals that the element in question can be inserted at a particular position, then space is created for it by shifting the other elements one position to the right and inserting the element at the suitable position.
3. The above procedure is repeated until all the element in the array is at their apt position.

### Performance 

##### Worst case performance : O(n<sup>2</sup>) 
Not appropriate for large unsorted data sets 

##### Average case performance: O(n<sup>2</sup>) 
Not appropriate for large unsorted data sets 

##### Best case performance: O(n) 
Very good best performance and can efficiently sort small and nearly sorted data sets 

##### Space required: O(N) 
Bubble sort operates directly on the input array meaning it is a candidate algorithm when minimizing space is paramount 

### Algorithm

We assume list is an array of n elements. We further assume that swap function swaps the values of the given array elements. 

````
INSERTION-SORT(A)
   for i = 1 to n
   	key ← A [i]
    	j ← i – 1
  	 while j > = 0 and A[j] > key
   		A[j+1] ← A[j]
   		j ← j – 1
   	End while 
   	A[j+1] ← key
  End for 
````