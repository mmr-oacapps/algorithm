package com.mmr.tutorial.sorting_algorithms.insertion_sort;

import java.util.Arrays;

public class InsertionSort {

    public static void main(String[] args) {
        int array[] = {3, 8, 2, 1, 5, 4, 6, 7};

        System.out.println("Before Sort : " + Arrays.toString(array));

        insertionSort(array);

        System.out.println("After Sort : " + Arrays.toString(array));
    }



   static void insertionSort(int[]  arr){

        int n = arr.length;

        /** Looping through the array length **/
        for (int i = 1; i < n; ++i)
        {
            int key = arr[i];
            int j = i-1;

            /** Move elements of arr[0..i-1],
             * that are greater than key,
             * to one position ahead of their current position **/
            while (j>=0 && arr[j] > key)
            {
                arr[j+1] = arr[j];
                j = j-1;
            }
            arr[j+1] = key;
        }
    }
}
