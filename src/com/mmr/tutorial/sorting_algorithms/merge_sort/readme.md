## Data process from start to end or left to right. 
- It is a divide and conquer sorting algorithm.


### Merge Sort works as follows:
- recursively split in half
- when an array have 1 item , it considered as sorted and reconstructed in sort order
- Each reconstructed array is merged with he other half

### Performance 

##### Worst case performance : O(n log n) 
- Appropriate for large data sets
- Data splitting means that the algorithm can be made parallel.


##### Average case performance:  O(n log n) 
- Appropriate for large data sets

##### Best case performance: O(n log n) 
- Appropriate for large data sets


##### Space required: O(n) 
- Merge can be, but is often not, performed in-place. These extra allocations increase the memory footprint required to sort data.


### Algorithm


````
MergeSort(arr[], l,  r)
If r > l
     Find the middle point to divide the array into two halves:  
        middle m = (l+r)/2
     Call mergeSort for first half:   
        Call mergeSort(arr, l, m)
     Call mergeSort for second half:
        Call mergeSort(arr, m+1, r)
     Merge the two halves sorted in step 2 and 3:
        Call merge(arr, l, m, r)
````