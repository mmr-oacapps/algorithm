package com.mmr.tutorial.sorting_algorithms.selection_sort;

import java.util.Arrays;

public class SelectionSort {

    public static void main(String[] args) {
        int array[] = {3, 8, 2, 1, 5, 4, 6, 7};

        System.out.println("Before Sort : " + Arrays.toString(array));

        selectionSort(array);

        System.out.println("After Sort : " + Arrays.toString(array));
    }

    static void selectionSort(int[] arr) {

        int n = arr.length;

        int minIndex, tempValue;

        // 1. looping full array
        for (int i = 0; i < n; i++)
        {
            // init index as min index
            minIndex    = i;

            // Start looping for current (i) position as starting for next loop
            for (int j = i+1; j < n; j++) {

                // determine if current J value is less than current i value
                if (arr[j] < arr[minIndex]) {
                    // get new index of smallest value
                    minIndex = j;
                }
            }

            tempValue = arr[minIndex];
            arr[minIndex] = arr[i];
            arr[i] = tempValue;

        }

    }


}
