## Data process from start to end or left to right. 
- It is a linear sorting algorithm.


### Selection Sort works as follows:
- It is kind of hybrid between bubble and insertion sort
- It works by finding smallest item in the collection and moving it to its appropriate location.
- It sort smallest to largest and it does without revisiting sorted data

### Performance 

##### Worst case performance : O(n<sup>2</sup>) 
Not appropriate for large unsorted data sets 

##### Average case performance: O(n<sup>2</sup>) 
- Not appropriate for large unsorted data sets 
- Typically performs better than bubble but worse than insertion sort
##### Best case performance: O(n<sup>2</sup>) 
- Very good best performance and can efficiently sort small and nearly sorted data sets 


##### Space required: O(N) 
- Selection sort operates directly on the input array meaning it is a candidate algorithm when minimizing space is paramount 


### Algorithm
- Enumerate the array from the first unsorted item to the end
- Identify the smallest item
- Swap the smallest item with the first unsorted item.

````
SELECTION-SORT(A)
    for j ← 1 to n-1
        smallest ← j
            for i ← j + 1 to n
                if A[ i ] < A[ smallest ]
                    smallest ← i
                    Exchange A[ j ] ↔ A[ smallest ]
````